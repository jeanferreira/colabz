import React, {useState, useEffect} from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Box, BoxTop } from './styles';
import { SmallText, TextNormal } from '../../components/TextStyles';
import { SimpleButton, TextButton } from '../../components/Button';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import CardPedidos from '../../components/CardPedidos/index';
import ListFooter from '../../components/ListFooter';

import api from '../../services/api';

export default function TabLiberados() {
  //Estado para modificar quando o botão de voltar ao topo deve ou não aparecer
  const [visibleFooter,setVisibleFooter] = useState('none');
  const [total,setTotal] = useState(0);
  const [textFooter,setTextFooter] = useState('arraste para ver mais');
  
  const [projects,setProjects] = useState([]);

  useEffect(() => {
    loadProjects();
  },[]);

  useEffect(() => {
    setVisibleFooter(projects.length === 0 ? 'none' : 'flex');
    setTextFooter(projects.length === total ? 'arraste para ver mais' : 'não há mais pedidos');
  }, [projects]);

  async function loadProjects(){
    const response = await api.get('/projects');
  }

  return (
    <FlatList
      style={styles.list}
      data={projects}
      keyExtractor={project => String(project.project_id)}
      ListEmptyComponent={() => {
        return(
          <View style={styles.empty}>
            <SmallText>Não há pedidos liberados</SmallText>
            <FontAwesome name='minus' color='#CCC'/>
          </View>
        )}
      }
      renderItem={ ({item: project}) => (
          <View style={styles.view}>
            <CardPedidos
              email={project.email}
              name={project.name}
              address={project.address}
              location={project.location}
              openDetailsProject={() => {navigation.navigate('PedidoDetalhado',{Pedido: project})}}
            />
          </View>
        )
      }
      onEndReached={() => {}}
      onEndReachedThreshold={0.2}
      ListFooterComponent={<ListFooter mensagem={textFooter}/>}
      ListFooterComponentStyle={{ display: visibleFooter}}
    />
  );
}

const styles = StyleSheet.create({
  list: {
    backgroundColor: '#FFF',
  },
  view: {
    flex: 1,
    paddingHorizontal: 20,
    paddingBottom: 10,
  },
  empty: {
    paddingTop: 20,
    alignItems: 'center',
  }
});