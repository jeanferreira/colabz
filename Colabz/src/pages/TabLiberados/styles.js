import styled from 'styled-components/native';

export const Box = styled.View`
    width: 100%;
    background-color: #FFF;
    margin-top: 20px;
    border-radius: 10px;
    padding: 15px;
    elevation: 5;
`;

export const BoxTop = styled.View`
    flex-direction: row;
    align-items: center;
`;