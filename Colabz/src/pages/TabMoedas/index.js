import React from 'react';
import { Scroll } from '../../components/Scroll';
import { TextNormal } from '../../components/TextStyles';
import { SimpleButton, TextButton } from '../../components/Button';
import { Container, Box, Rotulo } from './styles';

export default function Moedas() {
  return (
    <Scroll>
      <Container>
        <TextNormal marginBottom center style={{ width: '90%' }}>Consiga milhares de clientes para os seus serviços e aumente o seu faturamento! Sem mensalidades e a grana do serviço é toda sua!</TextNormal>

        <Box>
          <Rotulo>pacote 1</Rotulo>
          <TextNormal bold marginVertical>140 moedas - R$70,00</TextNormal>
          <SimpleButton purple overallWidth>
            <TextButton>comprar</TextButton>
          </SimpleButton>
        </Box>

        <Box>
          <Rotulo>pacote 2</Rotulo>
          <TextNormal boldd marginVertical>300 moedas - R$120,00</TextNormal>
          
          <Box pinkBorder>
            <Rotulo pink>Pacote mais vendido</Rotulo>
            <TextNormal bold marginVertical>compre e ganhe:</TextNormal>
            <TextNormal bold>40 moedas gratis</TextNormal>
            <TextNormal bold>+</TextNormal>
            <TextNormal bold>R$50 de desconto</TextNormal>
          </Box>
          
          <SimpleButton purple overallWidth>
            <TextButton>comprar</TextButton>
          </SimpleButton>
        </Box>

      </Container>
    </Scroll>
  );
}