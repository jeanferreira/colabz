import styled, { css } from 'styled-components/native';

export const Container = styled.View`
    align-items: center;
    width: 100%;
    height: 100%;  
    margin-bottom: 60px;
`;

export const Box = styled.View`
    background-color: #FFF;
    border-radius: 10px;
    margin: 20px 0;
    width: 90%;
    padding: 22px 15px 22px 15px;
    align-items: center;
    elevation: 5;

    ${props => props.pinkBorder && css`
        border-color: #FF96A4;
        border-width: 2px;
        elevation: 0;
    `}
`;

export const Rotulo = styled.Text`
    background-color: #444;
    color: #FFF;
    padding: 5px 20px;
    border-radius: 20px;
    position: absolute;
    top: -15px;
    font-family: 'Montserrat Medium';
    font-size: 13px;

    ${props => props.pink && css`
        background-color: #FF96A4;
    `}
`;