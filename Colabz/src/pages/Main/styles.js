import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  background-color: #FFF;
`;

export const Link = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    height: 63px;
`;

export const Box = styled.View`
  flex: 1;
  position: relative;
  justify-content: center;
  padding: 40px 40px 10px 40px;
`;