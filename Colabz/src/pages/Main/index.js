import React from 'react';
import { Container, Link, Box } from './styles';
import { HightDesc, TextNormal } from '../../components/TextStyles';
import { BoxHorizontal, BoxVertical, BoxAlignCenter } from '../../components/BoxOrientation';
import { Button, TextButton } from '../../components/Button';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function Main({navigation}) {
  const linkSiteColabz = 'https://www.colabz.com.br';

  return (
    <Container>
      <Box>
        <HightDesc>encontre profissionais</HightDesc>
        <HightDesc style={{ marginBottom: 15, color: '#FF96A4' }}>na sua região</HightDesc>

        <BoxVertical>
          <BoxHorizontal style={{ marginBottom: 5 }}>
            <FontAwesome5 name="check" size={20} color="#75E4C4" style={{ marginRight: 5 }}/>
            <TextNormal>4 orçamentos em até 1 hora</TextNormal>
          </BoxHorizontal>

          <BoxHorizontal>
            <FontAwesome5 name="check" size={20} color="#75E4C4" style={{ marginRight: 5 }}/>
            <TextNormal>profissionais avaliados</TextNormal>
          </BoxHorizontal>
        </BoxVertical>
      </Box>

      <BoxAlignCenter style={{ backgroundColor: '#FFF' }}>
          <Button purple onPress={()=>{navigation.navigate('MyWebView', {url: linkSiteColabz})}}>
            <TextButton>solicitar orçamento grátis</TextButton>
          </Button>
          <Link onPress={()=>{navigation.replace('Login')}}>
            <BoxHorizontal>
              <TextNormal>sou prestador de serviço</TextNormal>
              <FontAwesome5 name="long-arrow-alt-right" size={30} color="#444" style={{marginLeft: 10, marginBottom: -3}}/>
            </BoxHorizontal>
          </Link>
      </BoxAlignCenter>
    </Container>
  );
}