import React from 'react';
import { WebView } from 'react-native-webview';

export default function MyWebView({navigation}){

    const URL = navigation.getParam('url');

    return(
        <WebView style={{ flex: 1}} source={{uri: URL}} />
    ); 
}
