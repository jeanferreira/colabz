import React, { useState, useEffect } from 'react';
import { TextNormal, SmallText } from '../../components/TextStyles';
import { Button, TextButton } from '../../components/Button';
import { BoxHorizontal } from '../../components/BoxOrientation';
import { Container, Box, BoxItem, Contatos, Desbloquear, Flutuar, Alinhar, HeaderAlert, Back} from './styles';
import { StatusBar, Image, Linking } from  'react-native';
import { Scroll } from '../../components/Scroll';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import currencyIcon from '../../assets/images/currency/currency.png';
import largeCurrencyIcon from '../../assets/images/largeCurrency/currency.png';

import ModalAviso from '../../components/ModalAviso';
import Pedidos from '../TabPedidos';

export default function PedidoDetalhado({navigation}) {
  //Objeto com constantes recebidas da lista de pedidos que preencherão os dados desta página
  const Pedido = navigation.getParam('Pedido');

  const [liberado,setLiberado] = useState(Pedido.liberado);
  const [statusMoeda,setStatusMoeda] = useState(false);
  const [confirmado,setConfirmado] = useState(false);
  //Estados para serem alterados quando o pedido for atendido
  const [headerColor,setHeaderColor] = useState('#9F8CFF');
  const [textHeader,setTextHeader] = useState('atenda este pedido antes que finalize');
  const [textHeaderColor,setTextHeaderColor] = useState('#FFF');
  const [colorBackIcon,setColorBackIcon] = useState('#FFF');
  const [telefone,setTelefone] = useState('(**) *****-****');
  const [email,setEmail] = useState('*********@********');
  //Estado para exibir/ocultar modal
  const [visibleModal,setVisibleModal] = useState(false);
  //Variavel para simular as moedas
  const [moedas,setMoedas] = useState(100);
  //Estados para modificar o estilo do modal
  const [mensagem,setMensagem] = useState('quer liberar\neste pedido?');
  const [textoBotao,setTextoBotao] = useState('confirmar');
  const [corBotao,setCorBotao] = useState('#75e4c4');
  const [corTexto,setCorTexto] = useState('#FFF');
  const [visibilidadeBotaoFechar,setVisibilidadeBotaoFechar] = useState('flex');
  const [visibilidadeIcone,setVisibilidadeIcone] = useState('none');
  const [iconName,setIconName] = useState('check-circle');
  const [corIcon,setCorIcon] = useState('#75E4C4');
  const [boxAltura,setBoxAltura] = useState(60);
  const [backgroundStatusBarModal,setBackgroundStatusBarModal] = useState('rgba(0,0,0,0)');
  //Estados para serem alterados quando o pedido for liberado
  const [visibleItensContatos,setVisibleItensContatos] = useState('flex');
  const [visibleWhatsappIcon,setVisibleWhatsappIcon] = useState('none');
  const [textButtonLiberar,setTextButtonLiberar] = useState('liberar pedido');
  const [colorButtonLiberar,setColorButtonLiberar] = useState('#FF96A4');
  const [borderContatos,setBorderContatos] = useState(1);
  const [bottomContatos,setBottomContatos] = useState(20);
  const [paddingTopAviso,setPaddingTopAviso] = useState(StatusBar.currentHeight);//Estado apenas para celular com nothe

  //Para compor o email
  const [bodyMessage,setBodyMessage] = useState('Olá, tudo bem? Eu sou profissional da Colabz e recebi o seu pedido para poder te atender.\nTenho muito interesse em te ajudar, poderia me passar mais detalhes para eu te fazer um orçamento e realizar o seu serviço?');

  //Para alterar o background da barra de status do modal, deixar mais escura assim como background
  useEffect(() => {
    setBackgroundStatusBarModal(visibleModal ? 'rgba(0,0,0,0.5)' : 'rgba(0,0,0,0)');
  },[visibleModal]);

  useEffect(() => {
    if(liberado){
      estiloModalPedidoLiberado();
    }
  },[]);

  useEffect(() => {
    if(liberado)
      releaseContacts();
  },[liberado])

  //Função com as condições necessárias para o modal ser chamado
  //A partir daqui que é feito redirecionamento para suas devidas funções
  //de acordo com a condição (liberar pedido, comprar moeda, ou whatsapp)
  function releaseProject() {
    if(liberado){
      setVisibleModal(false);
      sendWhatsapp();
    } else if(confirmado){
      estiloModalPedidoLiberado();
      setVisibleModal(true);
      setLiberado(true);

      setMoedas(moedas-20);
    } else if(moedas >= 20 && !statusMoeda){
      estiloModalSolicitarPedido();
      setVisibleModal(true);
      setStatusMoeda(true);
      setConfirmado(true);
    } else if(moedas < 20 && !statusMoeda){
      estiloModalSemMoedas();
      setVisibleModal(true);
      setConfirmado(false);
      setStatusMoeda('comprar');
    } else {
      setVisibleModal(false);
      navigation.navigate('TabMoedas');
    }
  }

  //Função para exibir contatos do cliente após o pedido ser liberado
  function releaseContacts(){
    setTelefone(Pedido.phone);
    setEmail(Pedido.email);
  }

  //Função disparada após usuários cancelar a liberação do pedido
  //impede que os estados permaneçam ativos
  function close(){
    setStatusMoeda(false);
    setConfirmado(false);
    setLiberado(false);
    setVisibleModal(false);
  }

  //Estilo para ser alterado no pedido detalhado
  function estiloPedidoDetalhadoLiberado(){
    setVisibleModal(false);
  }

  function estiloModalSolicitarPedido(){
    setMensagem('quer liberar\neste pedido?');
    setTextoBotao('confirmar');
    setCorBotao('#75E4C4');
    setCorTexto('#FFF');
    setVisibilidadeBotaoFechar('flex');
    setVisibilidadeIcone('none');
    
    // setIconName('');
    // setCorIcon('');
    setBoxAltura(30);
  }

  //Estilização para ser enviado ao modal quando o pedido for liberado
  function estiloModalPedidoLiberado() {
    //Altera as cores para verde e o texto do cabecario
    setTextHeader('pedido liberado, aproveite e entre em contato');
    setHeaderColor('#75E4C4');
    // setBarStyleColor('dark-content');
    // setTextHeaderColor('#444');
    // setColorBackIcon('#444');

    //Altera o estado do botao para permitir enviar mensagem via whatsapp
    //libera a visualização dos contatos
    //e altera as cores para verde
    setVisibleItensContatos('none');
    setVisibleWhatsappIcon('flex');
    setTextButtonLiberar('enviar mensagem');
    setColorButtonLiberar('#75E4C4');
    setBorderContatos(0);
    setBottomContatos(0);

    setMensagem('pedido liberado\ncom sucesso');
    setTextoBotao('entrar em contato');
    setCorBotao('#FFF');
    setCorTexto('#444');
    setVisibilidadeBotaoFechar('none');
    setVisibilidadeIcone('flex');
    setBoxAltura(40);
  }

  //Estilização para ser enviado ao modal quando não houver moedas
  function estiloModalSemMoedas() {
    setMensagem('ops! suas moedas\nacabaram');
    setTextoBotao('comprar moedas');
    setCorBotao('#9F8CFF');
    setCorTexto('#FFF');
    setVisibilidadeBotaoFechar('flex');
    setVisibilidadeIcone('flex');
    setIconName('sad-tear');
    setCorIcon('#9F8CFF');
    setBoxAltura(30);
  }

  async function sendEmail(){
    Linking.openURL(`mailto:${Pedido.email}?subject=${Pedido.name}&body=${bodyMessage}`);
  }

  function sendWhatsapp(){
    Linking.openURL(`whatsapp://send?phone=${Pedido.phone}&text=${bodyMessage}`);
  }

  return (
    <>
      <StatusBar 
        animated={true} 
        showHideTransition='fade' 
        backgroundColor='rgba(0,0,0,0)'
        barStyle='light-content'
        translucent={true}
      />

      <HeaderAlert style={{backgroundColor: headerColor, paddingTop: paddingTopAviso}}>
        <Back onPress={() => {navigation.goBack()}}>
          <FontAwesome5 
            name='chevron-left' 
            color={colorBackIcon} 
            size={18}
          />
        </Back>
        <TextNormal bold center style={{color: textHeaderColor, flex: 1 }} numberOfLines={1}>{textHeader}</TextNormal>
      </HeaderAlert>

      <Scroll style={{paddingTop: 0}}>
        <Container>
          <Box>
            <TextNormal>projeto:</TextNormal>
            <SmallText>{Pedido.name}</SmallText>
          </Box>

          <Box>
            <TextNormal>local do serviço:</TextNormal>
            <SmallText>{Pedido.address}, {Pedido.location}</SmallText>
            
            <BoxItem style={{ alignItems: 'center' }}>
              <TextButton black>{Pedido.Propostas}/4 orçamentos</TextButton>
            </BoxItem>
          </Box>

          <Box>
            <TextNormal>descrição do pedido:</TextNormal>
            <SmallText>{Pedido.description}</SmallText>
          </Box>

          <Box>
            <TextNormal>prazo de serviço?</TextNormal>
            <SmallText></SmallText>
          </Box>

          <Box>
            <TextNormal>orçamentos já recebidos: </TextNormal>
            <SmallText>Libere antes que finalize</SmallText>
          </Box>

          <Box
            style={{
              borderBottomWidth: 0, 
              marginBottom: 0,
              paddingBottom: 0,
            }}
          >
            <TextNormal>contatos do(a) cliente</TextNormal>
            
            <BoxItem>
              <Box style={{
                  borderBottomWidth: borderContatos,
                  paddingBottom: bottomContatos,
                  marginBottom: bottomContatos,
                }}
              >
                <BoxHorizontal>
                  <Contatos>
                    <TextNormal style={{marginBottom: 10}}>{Pedido.firstname} {Pedido.lastname}</TextNormal>
                    
                    <BoxHorizontal style={{marginBottom: 0}}>
                      <FontAwesome5 name='mobile-alt' color='#AAA' style={{marginRight: 5}}/>
                      <SmallText>{telefone}</SmallText>
                    </BoxHorizontal>

                    <BoxHorizontal style={{marginBottom: 0}}>
                      <FontAwesome5 name='at' color='#AAA' style={{marginRight: 5}}/>
                      <SmallText>{email}</SmallText>
                    </BoxHorizontal>
                  </Contatos>

                  <Desbloquear
                    onPress={() => releaseProject()}
                    style={{
                      display: visibleItensContatos,
                    }}
                  >
                    <FontAwesome5 name='lock' size={100} color='#444'/>
                    <Flutuar>
                      <Image source={currencyIcon}/>
                      <TextNormal style={{color: '#FFF', fontSize: 20}}>20</TextNormal>
                    </Flutuar>
                  </Desbloquear>
                </BoxHorizontal>
              </Box>

              <Alinhar
                style={{
                  display: visibleItensContatos,
                }}
              >
                <SmallText style={{marginBottom: 0 }}>Contatos validados pelo nosso sistema.</SmallText>
                <SmallText style={{marginTop: 0}}>Atenda antes que este pedido finalize.</SmallText>
              </Alinhar>
            </BoxItem>

            <BoxItem
              style={{
                marginBottom: 20,
                alignItems: 'center',
                display: visibleItensContatos,
              }}
            >
              <BoxHorizontal>
                <Image source={largeCurrencyIcon} style={{marginRight: 8}}/>
                <TextButton style={{color: '#444'}}>minhas moedas: {moedas}</TextButton>
              </BoxHorizontal>
            </BoxItem>

            <Button
              onPress={() => releaseProject()}
              style={{
                backgroundColor: colorButtonLiberar,
                width: '100%',
              }}
            >
              <BoxHorizontal>
                <FontAwesome5
                  name='whatsapp'
                  size={30}
                  color='#FFF'
                  style={{ 
                    display: visibleWhatsappIcon,
                    marginRight: 10,
                  }}
                />
                <TextButton>{textButtonLiberar}</TextButton>
              </BoxHorizontal>
            </Button>

            <Button pink
              onPress={sendEmail}
              style={{
                width: '100%',
                display: visibleWhatsappIcon,
                marginTop: 10,
              }}
            >
              <BoxHorizontal>
                <FontAwesome5
                  name='envelope'
                  size={30}
                  color='#FFF'
                  style={{
                    marginRight: 10,
                  }}
                />
                <TextButton>enviar e-mail</TextButton>
              </BoxHorizontal>
            </Button>
          </Box>

          <ModalAviso
            visibilidadeIcone={visibilidadeIcone}
            iconName={iconName}
            corIcon={corIcon}
            mensagem={mensagem}
            textoBotao={textoBotao}
            corBotao={corBotao}
            corTexto={corTexto}
            visibilidadeBotaoFechar={visibilidadeBotaoFechar}
            boxAltura={boxAltura}
            visibilidadeSubMensagem='none'
            backgroundStatusBar={backgroundStatusBarModal}

            actionButton={() => {
              releaseProject();
            }}

            close={() => {
              close();
            }}

            visibleModal={visibleModal} 
            setVisibleModal={setVisibleModal}
            liberado={liberado}
            setConfirmado={setConfirmado}
            setStatusMoeda={setStatusMoeda}
          />
        </Container>
      </Scroll>
    </>
  );
}

