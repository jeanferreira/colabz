import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #FFF;
  padding: 20px;
`;

export const HeaderAlert = styled.View`
  flex-direction: row;
  width: 100%;
  height: 100px;
  elevation: 5;
  align-items: center;
`;

export const Box = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #DDD;
  padding-bottom: 20px;
  margin-bottom: 30px;
`;

export const BoxItem = styled.View`
  background-color: #FFF;
  border-radius: 10px;
  padding: 20px;
  margin-bottom: 20px;
  width: 100%;
  elevation: 5;
  margin: 15px 0px;
`;

export const Contatos = styled.View`
  flex: 1;
  padding-right: 10px;
`;

export const Desbloquear = styled.TouchableOpacity`

`;

export const Flutuar = styled.View`
  position: absolute;
  width: 55%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  top: 60%;
  left: 23.5%;
`;

export const Alinhar = styled.View`
  align-items: center;
  width: 100%;
`;

export const Back = styled.TouchableOpacity`
  padding: 19px 10px 19px 15px;
`;