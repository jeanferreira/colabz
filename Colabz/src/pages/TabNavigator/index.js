import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Home from '../Home';
import Pedidos from '../Pedidos';
import Moedas from '../Moedas';

const TabNavigator = createBottomTabNavigator(
    {
        Colabz: {
            screen: Home,
        },
        Pedidos: {
            screen: Pedidos,
        },
        Moedas: {
            screen: Moedas,
        },
    },
    {
        tabBarOptions: {
            activeTintColor: '#FF96A4',
            inactiveTintColor: '#AAA',
            activeBackgroundColor: '#444',
            inactiveBackgroundColor: '#FFF',
            style: {
                backgroundColor: '#FAFAFA',
                borderStyle: 'dotted',
                height: 55,
                borderTopWidth: 0,
            },
            labelStyle: {
                fontSize: 13,
                fontFamily: 'Montserrat Regular',
                justifyContent: 'center',
            },
            tabStyle: {
                // borderTopLeftRadius: 10,
                // borderTopRightRadius: 10,
                elevation: 10,
                paddingVertical: 5,
                // marginHorizontal: 1,
            }
        },
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName, } = navigation.state;
                let IconComponent = FontAwesome5;
                let iconName;
                let colorIcon;

                if (routeName === 'Colabz'){
                    iconName = 'home';
                    colorIcon = (focused) ? '#FF96A4' : '#DDD';

                } else if (routeName === 'Pedidos'){
                    iconName = 'handshake';
                    colorIcon = (focused) ? '#FF96A4' : '#DDD';

                } else if (routeName === 'Moedas'){
                    iconName = 'dollar-sign';
                    colorIcon = (focused) ? '#FF96A4' : '#DDD';
                }

                return <IconComponent name={iconName} solid={true} size={25} color={colorIcon} />
            }
        }),
    }
);

export default createAppContainer(TabNavigator);
