import styled from 'styled-components/native';

export const Container = styled.View`
    background-color: #FFF;
    flex: 1;
    padding: 20px;
    align-items: center;
`;

export const Box = styled.View`
    background-color: #FFF;
    border-radius: 10px;
    padding: 30px 20px;
    margin-bottom: 20px;
    width: 100%;
    align-items: center;
    elevation: 5;
`;

export const Sair = styled.TouchableOpacity`
    position: absolute;
    bottom: 10px;
    align-items: center;
`;

//Estilo para o modal
export const ContainerModal = styled.View`
    flex: 1;
    background: rgba(0,0,0,0.5);
    align-items: center;
    justify-content: center;
`;

export const BoxModal = styled.View`
    width: 285px;
    background-color: #FFF;
    border-radius: 10px;
    elevation: 10;
    justify-content: center;
    align-items: center;
    padding-left: 30px;
    padding-right: 30px;
`;

export const Stars = styled.View`
    flex-direction: row;
    width: 90%;
    justify-content: space-around;
    margin-bottom: 30px;
`;

export const CloseModal = styled.TouchableOpacity`
    margin: 20px 0;
`;