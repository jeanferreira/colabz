import React, { useState, useEffect } from 'react';
import { StyleSheet, Modal, Image, TouchableOpacity, StatusBar, Linking } from 'react-native';
import { TextNormal, SmallText } from '../../components/TextStyles';
import { Button, TextButton } from '../../components/Button';
import { Container, Box, Sair, ContainerModal, BoxModal, CloseModal, Stars  } from './styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import logo from '../../assets/images/icon/icon.png';

export default function Moedas({navigation}) {
  const helpURL = 'https://www.colabz.com.br/ajuda';
  const politicURL = 'https://www.colabz.com.br/termos-e-privacidade';
  const profileURL = 'http://www.colabz.com.br/profissionais';

  const [visibleModal,setVisibleModal] = useState(false);
  const [solidIcon,setSolidIcon] = useState(false);
  const [value,setValue] = useState(0);
  const [stars,setStars] = useState([
    {id: 1, active: false},
    {id: 2, active: false},
    {id: 3, active: false},
    {id: 4, active: false},
    {id: 5, active: false},
  ]);

  useEffect(() => {
    StatusBar.setBackgroundColor(visibleModal ? '#7F7F7F' : '#FFF');
  },[visibleModal]);

  function renderizarEstrelas(){
    return (<TextNormal bold>oi</TextNormal>);
  }

  function preencherEstrelas(){
    // stars.map(){

    // }
  }

  function rateMe(e){
    let sts = [];
    let i = 1;

    for(; i<=5; i++){
      if(i <= e)
        sts.push({id: i, active: true});
      else
        sts.push({ id: i, active: false});
    }

    setStars(sts);

    setTimeout(() => {
      Linking.openURL('market://details?id=br.com.colabz');
    },350);
  }

  return (
    <Container>
      <Box>
        <TextNormal bold marginBottom style={{fontSize: 18}}>Ana Paula</TextNormal>

        <Button style={styles.button} onPress={() => navigation.navigate('MyWebView',{url: profileURL})}>
          <TextButton black>editar minha página</TextButton>
        </Button>

        <Button style={styles.button} onPress={() => navigation.navigate('MyWebView',{url: helpURL})}>
          <TextButton black>precisa de ajuda?</TextButton>
        </Button>

        <Button style={styles.button} onPress={() => navigation.navigate('MyWebView',{url: politicURL})}>
          <TextButton black>política de privacidade</TextButton>
        </Button>
      </Box>

      <Button pink
        onPress={() => setVisibleModal(true)}
        style={styles.button}
      >
        <TextButton>avalie nosso aplicativo</TextButton>
      </Button>

      <Sair onPress={() => {navigation.replace('Main')}}>
        <FontAwesome5 name='power-off' color='#444' size={18}/>
        <SmallText bold>sair</SmallText>
      </Sair>

      <Modal
        visible={visibleModal}
        animationType={"slide"}
        transparent={true}
        onRequestClose={() => setVisibleModal(false)}
      >
        <ContainerModal>
          <BoxModal>
            <Image source={logo} style={{ marginVertical: 25 }}/>

            <SmallText bold style={{ marginBottom: 0, marginTop: 0 }}>Curtindo a Colabz?</SmallText>
            <SmallText centralizado style={{ marginBottom: 0, marginTop: 0 }}>Toque em uma estrela para</SmallText>
            <SmallText centralizado style={{ marginBottom: 30, marginTop: 0 }}>avaliar na PlayStore.</SmallText>
            
            <Stars>
              {stars.map((star) => (
                  <TouchableOpacity key={star.id} onPress={() => rateMe(star.id) }>
                    <FontAwesome5 name='star' solid={star.active} size={27} color='#484848'/>
                  </TouchableOpacity>
                )
              )}
            </Stars>

            <CloseModal onPress={() => setVisibleModal(false)}>
              <SmallText>Agora não</SmallText>
            </CloseModal>
          </BoxModal>
        </ContainerModal>
      </Modal>
    </Container>
  );
}

const styles = StyleSheet.create({
  button: {
    marginVertical: 10,
    width: '100%',
    height: 50,
  },
})