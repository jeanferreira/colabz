import React, { useState, useEffect } from 'react';
import { StyleSheet, KeyboardAvoidingView, StatusBar } from 'react-native';
import { TextNormal, HightDesc, SmallText } from '../../components/TextStyles';
import { Input } from '../../components/Input';
import { Container, Link, BoxSenha, Ocultar } from './styles';
import { Button, TextButton } from '../../components/Button';
import { BoxHorizontal, BoxEnd, BoxVertical, BoxTop, BoxCenter } from '../../components/BoxOrientation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import ModalAviso from '../../components/ModalAviso';

import api from '../../services/api';

export default function Login({navigation}) {
    const linkCadastroSite = 'https://mailchi.mp/5a26c37e6744/cadastro-profissionais';

    //Variáveis que alteram o estado das bordas dos campos
    //e visibilidade do texto de alerta
    const [borderWidthEmail,setBorderWidthEmail] = useState(0);
    const [borderWidthPassword,setBorderWidthPassword] = useState(0);
    const [visibleTextAlert,setVisibleTextAlert] = useState('none');
    const [textAlert,setTextAlert] = useState('acesso incorreto, digite novamente, por favor');

    //Variáveis que alteram o estado do ícone
    //e do texto da senha, ocultando ou exibindo
    const [hiddenPassword, setHiddenPassword] = useState(true);
    const [iconName,setIconName] = useState('eye');
    
    //Variáveis que alteram o estado da interface
    //quando o usuário seleciona que esqueceu a senha
    const [visiblePasswordInput,setVisiblePasswordInput] = useState('flex');
    const [textButton,setTextButton] = useState('entrar');
    const [title1,setTitle1] = useState('entre na sua');
    const [title2,setTitle2] = useState('conta de profissional');
    const [textLinkRecovePassword,setTextLinkRecovePassword] = useState('esqueceu sua senha?');
    const [loginMode,setLoginMode] = useState(true);

    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');

    //Estados para controlar a visibilidade do modal de confirmação de recuperação de senha
    const [visibleModal,setVisibleModal] = useState(false);

    useEffect(() => {
        StatusBar.setBackgroundColor(visibleModal ? '#7F7F7F' : '#FFF');
    },[visibleModal]);

    //função responsável por destacar as bordas do campo de email
    function setBorderEmail(destacar){
        setBorderWidthEmail(destacar ? 2 : 0 );
        setVisibleTextAlert(destacar ? 'flex' : 'none');
    }

    //função responsável por destacar as bordas do campo de senha
    function setBorderPassword(destacar){
        setBorderWidthPassword(destacar ? 2 : 0 );
        setVisibleTextAlert(destacar ? 'flex' : 'none');
    }

    //Função para alterar icone de visualização da senha
    function showPassword(){
        setHiddenPassword(hiddenPassword ? false : true);
        setIconName(hiddenPassword ? 'eye-slash' : 'eye');
    }

    //Função para ocultar o campo de senha
    //e alterar o titulo da página
    function recoverPasswordMode(){
        setBorderEmail(false);
        setBorderPassword(false);
        setLoginMode( loginMode ? false : true);
        setVisiblePasswordInput( loginMode ? 'none' : 'flex');
        setTextButton(loginMode ? 'recuperar' : 'entrar');
        setTitle1( loginMode ? 'esqueceu a' : 'entre na sua');
        setTitle2( loginMode ? 'senha?' : 'conta de profissional');
        setTextLinkRecovePassword( loginMode ? 'já possui uma conta?' : 'esqueceu sua senha?');
    }

    //Função para verificar se o login é válido
    function authentication(){
        if(loginMode){//Se estive no modo login
            if(email.length > 0 && password.length > 0){
                try {
                    const response = await api.post('/sessions',{email,password});
                    
                    if(response.data.length >  0){
                        // await setSession(...response.data);
                        navigation.replace('TabNavigator',...response.data);
                    } else {
                        console.log('entrou');
                        setTextAlert('acesso incorreto, digite novamente, por favor');
                        setVisibleTextAlert('flex');
                    }
                } catch (error) {
                    setTextAlert('erro, verifique sua internet e tente novamente');
                    setVisibleTextAlert('flex');
                }
            } else if(email.length === 0 && password.length === 0){
                setTextAlert('por favor, preencha todos os campos');
                setVisibleTextAlert('flex');
                setBorderEmail(true);
                setBorderPassword(true);
            } else if(email.length === 0){
                setTextAlert('por favor, preencha o campo de email');
                setVisibleTextAlert('flex');
                setBorderEmail(true);
            }else if(password.length === 0){
                setTextAlert('por favor, preencha o campo de senha');
                setVisibleTextAlert('flex');
                setBorderPassword(true);
            }
        } else{//Se estiver no modo recuperar senha
            if(email.length > 0 /*&& se ele existir no banco de dados*/){
                setVisibleModal(true);
            // } else if(/*CASO O EMAIL NÃO EXISTA NO BANCO DE DADOS*/){
            //     setTextAlert('nenhum usuário com este e-mail foi encontrado');
            //     destacarBordas(true);
            } else {
                setTextAlert('por favor, preencha o campo de email');
                setBorderEmail(true);
            }
        }
    }

  return (
    <KeyboardAvoidingView behavior='height'>
    <Container>

        <BoxTop>
            <BoxVertical>
                <HightDesc medium>{title1}</HightDesc>
                <HightDesc medium>{title2}</HightDesc>
            </BoxVertical>
        </BoxTop>

        <BoxCenter>
            <SmallText bold style={{color: '#FF2E4A', display: visibleTextAlert}}>{textAlert}</SmallText>

            <Input
                placeholder="digite seu e-mail"
                keyboardType='email-address'
                textContentType='emailAddress'
                onChangeText={e => setEmail(e)}
                onFocus={() => setBorderEmail(false)}
                style={{
                    borderColor: '#FF2E4A',
                    borderWidth: borderWidthEmail,
                }}
            />

            <BoxSenha style={{ 
                display: visiblePasswordInput,
                borderColor: '#FF2E4A',
                borderWidth: borderWidthPassword,
            }}>
                <Input
                    placeholder="digite sua senha"
                    secureTextEntry={hiddenPassword}
                    onChangeText={text => setPassword(text)}
                    onSubmitEditing={authentication}
                    onFocus={() => setBorderPassword(false)}
                    style={{
                        flex: 1,
                        marginBottom: 0,
                        marginTop: 0,
                        elevation: 0,
                    }}
                />
                <Ocultar onPress={showPassword}>
                    <FontAwesome5
                        name={iconName}
                        solid={true}
                        color='#DDD'
                        size={18}
                    />
                </Ocultar>
            </BoxSenha>

            <Link
                style={{ marginVertical: 15 }}
                onPress={recoverPasswordMode}>
                <SmallText>{textLinkRecovePassword}</SmallText>
            </Link>
            
            <Button pink
                onPress={authentication}>
                <TextButton>{textButton}</TextButton>
            </Button>
            
            <Button style={{ marginTop: 20 }} onPress={()=>{navigation.replace('Main')}}>
                <BoxHorizontal>
                    <FontAwesome5 
                    name='angle-left' 
                    size={20} 
                    color='#444' 
                    style={{ 
                        marginBottom: -2,
                        marginRight: 8,
                    }}
                />
                    <TextButton black>voltar</TextButton>
                </BoxHorizontal>
            </Button>
        </BoxCenter>

        <BoxEnd>
            <Link onPress={() => {navigation.navigate('MyWebView', {url: linkCadastroSite})}}>
                <TextNormal semibold>Ainda não tem uma conta?</TextNormal>
                <TextNormal semibold>Cadastre seus serviços grátis</TextNormal>
            </Link>
        </BoxEnd>
        
        <ModalAviso
            visibilidadeIcone='flex'
            iconName='check-circle'
            corIcon='#75E4C4'
            mensagem='senha enviada para seu e-mail'
            textoBotao='acessar conta'
            corBotao='#444'
            corTexto='#FFF'
            visibilidadeBotaoFechar='none'
            boxAltura={30}
            subMensagem='(olhe também no espam e no lixo eletrônico)'
            visibilidadeSubMensagem='flex'

            actionButton={() => {
                recoverPasswordMode();
                setVisibleModal(false);
            }}

            visibleModal={visibleModal}
            setVisibleModal={setVisibleModal}
        />
    </Container>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
    back: {
        marginLeft: 10,
        fontSize: 16,
        color: "#444",
    },
})
