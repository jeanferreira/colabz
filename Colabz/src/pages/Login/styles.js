import styled from 'styled-components/native';

export const Container = styled.View`
  height: 100%;
  flex-direction: column;
  background-color: #FFF;
  align-items: center;
`;

export const Link = styled.TouchableOpacity`
    align-items: center;
`;

export const BoxSenha = styled.View`
  flex-direction: row;
  width: 300px;
  align-items: center;
  margin: 0 20px;
  background-color: #FFF;
  border-radius: 8px;
  elevation: 5;
  padding: 0;
`;

export const Ocultar = styled.TouchableOpacity`
  width: 50px;
  height: 55px;
  justify-content: center;
  align-items: center;
  right: 0;
  z-index: 10;
`;