import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import TabMoedas from '../TabMoedas/index';
import TabConta from '../TabConta/index';

const TabsMoedas = createMaterialTopTabNavigator(
  {
    TabMoedas: {
      screen: TabMoedas,
      navigationOptions: {
        tabBarLabel: 'moedas',
      },
    },
    TabConta: {
      screen: TabConta,
      navigationOptions: {
        tabBarLabel: 'sua conta',
      },
    },
  },
  {
    tabBarOptions: {
      pressColor: '#FF96A4',
      activeTintColor: '#444',
      inactiveTintColor: '#C8C8C8',
      upperCaseLabel: false,
      labelStyle:{
        fontSize: 16,
        fontFamily: 'Montserrat Bold',
      },
      style: {
        backgroundColor: '#FFF',
        justifyContent: 'center'
      },
      indicatorStyle: {
        backgroundColor: '#444',
      },
    },
    defaultNavigationOptions: {
      swipeEnabled: true,
      // lazy: true,
    },
  }
);

export default createAppContainer(TabsMoedas);