import React, { useState, useEffect } from 'react';
import { Image, Modal, StatusBar } from 'react-native';
import { TextNormal, SmallText, HightDesc } from '../../components/TextStyles';
import { SimpleButton, TextButton } from '../../components/Button';
import { Box, BoxItem, BoxDicas, Container, ContainerModal, BoxModal, BoxTitleModal } from './styles';
import { Scroll } from '../../components/Scroll';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import currencyIcon from '../../assets/images/currency/currency.png';

export default function Home({navigation}) {
    const [qtdPedidos,setQtdPedidos] = useState(6);
    const [qtdLiberados,setQtdLiberados] = useState(2);
    const [qtdMoedas,setQtdMoedas] = useState(30);

    const [visibleNoteModal,setVisibleNoteModal] = useState(false);
    const [backgroundStatusBarModal,setBackgroundStatusBarModal] = useState(null);
    const [paddingTop,setPaddingTop] = useState(StatusBar.currentHeight);

    useEffect(() => {
        setTimeout(() => {
            setVisibleNoteModal(true);
        },700);
    },[]);

    useEffect(() => {
        setBackgroundStatusBarModal(visibleNoteModal ? 'rgba(159,140,255,0.7)' : null);
        StatusBar.setTranslucent(visibleNoteModal ? true : false);
        setPaddingTop(visibleNoteModal ? StatusBar.currentHeight : 0);
    },[visibleNoteModal]);

  return (
    <>
    <StatusBar backgroundColor={backgroundStatusBarModal}/>
    <Scroll>
        <Container style={{paddingTop: paddingTop}}>
            <TextNormal bold marginBottom>Olá, {navigation.getParam('firstname')} {navigation.getParam('lastname')}!</TextNormal>
            
            <Box>
                <BoxItem>
                    <TextNormal bold large>novos pedidos de clientes</TextNormal>

                    <SimpleButton onPress={() => {navigation.navigate('TabPedidos')}}>
                        <TextButton>{qtdPedidos} pedidos</TextButton>
                        <FontAwesome5 
                            name='chevron-right'
                            size={18}
                            color='#FFF'
                            style={{
                                position: 'absolute',
                                right: 20,
                            }}
                        />
                    </SimpleButton>
                </BoxItem>

                <BoxItem>
                    <TextNormal bold large>meus pedidos liberados</TextNormal>

                    <SimpleButton onPress={() => {navigation.navigate('TabLiberados')}}>
                        <TextButton>{qtdLiberados} pedidos</TextButton>
                        <FontAwesome5 
                            name='chevron-right'
                            size={18}
                            color='#FFF'
                            style={{
                                position: 'absolute',
                                right: 20,
                            }}
                        />
                    </SimpleButton>
                </BoxItem>

                <BoxItem>
                    <TextNormal bold large>suas moedas: {qtdMoedas}</TextNormal>

                    <SimpleButton style={styles.button} onPress={() => {navigation.navigate('TabMoedas')}}>
                        <Image source={currencyIcon} style={{marginRight: 5}} />
                        <TextButton>comprar moedas</TextButton>
                    </SimpleButton>
                </BoxItem>
            </Box>

            <BoxItem style={{ top: 0, marginTop: 20, paddingLeft: 20, paddingRight: 20,}}>
                <TextNormal bold center large >vamos juntos criar a melhor</TextNormal>
                <TextNormal bold center large>rede de serviços do país</TextNormal>
                
                <SimpleButton pink large
                    onPress={() => {navigation.navigate('TabPedidos')}}
                >
                    <TextButton>ver novos pedidos</TextButton>
                </SimpleButton>
            </BoxItem>

            <BoxDicas>
                <FontAwesome5 name="check-circle" solid={true} color='#75E4C4' size={35}/>
                <TextNormal semibold center>ganhe 20 moedas grátis todos os meses</TextNormal>
            </BoxDicas>

            <BoxDicas>
                <FontAwesome5 name="check-circle" solid={true} color='#75E4C4' size={35}/>
                <TextNormal semibold center>grana toda sua, sem mensalidade ou comissões</TextNormal>
            </BoxDicas>
        </Container>

        <Modal
            visible={visibleNoteModal}
            animationType='fade'
            transparent={true}
            onRequestClose={() => setVisibleNoteModal(false)}
            statusBarTranslucent={false}
        >
            <ContainerModal>
                <BoxModal>
                    <BoxTitleModal>
                        <HightDesc medium>estamos em</HightDesc>
                        <HightDesc medium>fase de cadastro</HightDesc>
                        <HightDesc medium>de profissionais</HightDesc>
                    </BoxTitleModal>

                    <SmallText center>Em julho seus pedidos</SmallText>
                    <SmallText center>vão aparacer aqui.</SmallText>
                    <SmallText center>Por favor, aguarde.</SmallText>

                    <SimpleButton pink boxShadow style={{marginTop: 30}} onPress={() => setVisibleNoteModal(false)}>
                        <TextButton>fechar</TextButton>
                    </SimpleButton>
                </BoxModal>
            </ContainerModal>
        </Modal>
    </Scroll>
    </>
  );
}
