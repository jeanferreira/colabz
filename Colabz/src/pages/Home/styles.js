import styled from 'styled-components/native';

export const Container = styled.View`
    align-items: center;
    margin-bottom: 30px;
`;

export const Box = styled.View`
    background-color: #9B88F8;
    width: 100%;
    height: 375px;
    margin-top: 70px;
    align-items: center;
    elevation: 5;
`;

export const BoxItem = styled.View`
    background-color: #FFF;
    border-radius: 10px;
    margin: 0 10px;
    width: 90%;
    position: relative;
    top: -60px;
    padding: 22px 15px 22px 15px;
    align-items: center;
    margin-bottom: 25px;
    elevation: 5;
`;

export const BoxDicas = styled.View`
    margin: 20px 0;
    width: 55%;
    align-items: center;
`;

//Estilo para o modal
export const ContainerModal = styled.View`
    flex: 1;
    background: rgba(159,140,255,0.7);
    align-items: center;
    justify-content: center;
    padding: 0 20px;
`;

export const BoxModal = styled.View`
    width: 100%;
    height: 60%;
    padding: 55px 0;
    background-color: #FFF;
    border-radius: 10px;
    elevation: 10;
    justify-content: center;
    align-items: center;
    padding-left: 30px;
    padding-right: 30px;
`;

export const BoxTitleModal = styled.View`
    align-items: center;
    padding-bottom: 30px;
    margin-bottom: 30px;
    border-bottom-width: 1px;
    border-bottom-color: #ccc;
`;