import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import TabPedidos from '../TabPedidos/index';
import TabLiberados from '../TabLiberados/index';

const TabsPedidos = createMaterialTopTabNavigator(
  {
    TabPedidos: {
      screen: TabPedidos,
      navigationOptions: {
        tabBarLabel: 'pedidos',
      },
    },
    TabLiberados: {
      screen: TabLiberados,
      navigationOptions: {
        tabBarLabel: 'liberados',
      },
    },
  },
  {
    tabBarOptions: {
      pressColor: '#FF96A4',
      activeTintColor: '#444',
      inactiveTintColor: '#C8C8C8',
      upperCaseLabel: false,
      labelStyle:{
        fontSize: 16,
        fontFamily: 'Montserrat Bold',
      },
      style: {
        backgroundColor: '#FFF',
        justifyContent: 'center'
      },
      indicatorStyle: {
        backgroundColor: '#444',
        height: 3,
      },
    },
    defaultNavigationOptions: {
      swipeEnabled: true,
    },
  }
);

export default createAppContainer(TabsPedidos);