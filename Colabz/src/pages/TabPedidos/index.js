import React, { useState, useEffect } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Box, BoxTop, Indicador } from './styles';
import { SmallText, TextNormal } from '../../components/TextStyles';
import { SimpleButton, TextButton } from '../../components/Button';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import CardPedidos from '../../components/CardPedidos';
import ListFooter from '../../components/ListFooter';

import api from '../../services/api';

export default function Pedidos({navigation}) {
  //Estado para modificar quando o botão de voltar ao topo deve ou não aparecer
  const [visibleFooter, setVisibleFooter] = useState('none');
  //Estado para indicar quando há um pedido novo
  const [status, setStatus] = useState('flex');
  //Estados para gerencias a lista
  const [projects,setProjects] = useState([]);
  const [page,setPage] = useState(1);
  const [total,setTotal] = useState(0);
  const [loading,setLoading] = useState(false);
  const [textFooter,setTextFooter] = useState('arraste para ver mais');

  async function loadProjects(){
    if(loading){
      return;
    }

    if(total > 0 && projects.length === total){
      return;
    }

    setLoading(true);

    const response = await api.get('/projects',{
      params: { page },
      headers: { category_id: 34 }
    });

    await setProjects([...projects,...response.data]);
    setTotal(response.headers['x-total-count']);
    setPage(page+1);
    setLoading(false);
  }

  useEffect(() => {
    loadProjects();
  },[]);

  useEffect(() => {
    setVisibleFooter(projects.length === 0 ? 'none' : 'flex');
    setTextFooter(projects.length === total ? 'arraste para ver mais' : 'não há mais pedidos');
  }, [projects]);

  return (
    <FlatList
      style={styles.list}
      data={projects}
      keyExtractor={project => String(project.project_id)}
      ListEmptyComponent={() => {
        return(
          <View style={styles.empty}>
            <SmallText>Não há pedidos</SmallText>
            <FontAwesome name='minus' color='#CCC'/>
          </View>
        )}
      }
      renderItem={ ({item: project}) => (
          <View style={styles.view}>
            <CardPedidos
              email={project.email}
              name={project.name}
              address={project.address}
              location={project.location}
              openDetailsProject={() => {navigation.navigate('PedidoDetalhado',{Pedido: project})}}
            />
          </View>
        )
      }
      onEndReached={loadProjects}
      onEndReachedThreshold={0.2}
      ListFooterComponent={<ListFooter mensagem={textFooter}/>}
      ListFooterComponentStyle={{ display: visibleFooter}}
    />
  );
}

const styles = StyleSheet.create({
  list: {
    backgroundColor: '#FFF',
  },
  view: {
    flex: 1,
    paddingHorizontal: 20,
    paddingBottom: 10,
  },
  empty: {
    paddingTop: 20,
    alignItems: 'center',
  }
});