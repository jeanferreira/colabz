import React from 'react';
import { StatusBar, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { TextNormal } from './components/TextStyles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Main from './pages/Main';
import Login from './pages/Login';
import MyWebView from './pages/MyWebView';
import TabNavigator from './pages/TabNavigator';

import TabLiberados from './pages/TabLiberados';
import TabPedidos from './pages/TabPedidos';
import TabMoedas from './pages/TabMoedas';
import PedidoDetalhado from './pages/PedidoDetalhado';

import logo from './assets/images/logo/colabz.png';

const Routes = createAppContainer(
    createStackNavigator({
        Main: {
            screen: Main,
            navigationOptions: {
                headerTitle: () => <Image source={logo}/>
            }
        },
        Login: {
            screen: Login,
            navigationOptions: {
                headerShown: false,
            }
        },
        MyWebView:{
            screen: MyWebView,
            navigationOptions: {
                headerBackImage: () =>
                <> 
                    <FontAwesome5 name='chevron-left' color='#444' size={18} style={{ marginLeft: 10}}/>
                    <TextNormal style={{marginLeft:10}}>voltar</TextNormal>
                </>
                ,
                headerTitle: () => <TextNormal></TextNormal>
            }
        },
        TabNavigator: {
            screen: TabNavigator,
            navigationOptions: {
                headerShown: false,
            }
        },
        TabLiberados: {
            screen: TabLiberados,
            navigationOptions: {
                headerShown: false,
            }
        },
        TabPedidos: {
            screen: TabPedidos,
            navigationOptions: {
                headerShown: false,
            }
        },
        TabMoedas: {
            screen: TabMoedas,
            navigationOptions: {
                headerShown: false,
            }
        },
        PedidoDetalhado: {
            screen: PedidoDetalhado,
            navigationOptions: {
                headerShown: false,
            }
        }
    }, {
        initialRouteName: 'Main'
    })
);

export default function src() {
    return (
      <>
        <StatusBar animated={true} showHideTransition='fade' barStyle="dark-content" backgroundColor="#FFF"/>
        <Routes/>
      </>
    );
  }