import styled from 'styled-components/native';

export const BoxHorizontal = styled.View`
    flex-direction: row;
    align-items: center;
`;

export const BoxVertical = styled.View`
    flex-direction: column;
`;

export const BoxTop = styled.View`
    justify-content: center;
    height: 25%;
    width: 300px;
`;

export const BoxCenter = styled.View`
    justify-content: center;
    align-items: center;
    height: 50%;
    width: 300px;
`;

export const BoxEnd = styled.View`
    position: absolute;
    width: 100%;
    bottom: 20px;
`;

export const BoxAlignCenter = styled.View`
    width: 100%;
    padding: 40px 0;
    elevation: 50;
    align-items: center;
`;