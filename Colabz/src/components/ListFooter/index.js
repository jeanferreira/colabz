import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { SmallText } from '../../components/TextStyles';
export default class index extends Component {
    render() {
        return (
            <View style={styles.container}>
                <SmallText bold style={styles.mensagem}>{this.props.mensagem}</SmallText>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 30,
        backgroundColor: '#EEE',
        borderRadius: 20,
        marginTop: 20,
        marginHorizontal: 20,
    },
    mensagem: {
        color: '#BBB'
    },
})