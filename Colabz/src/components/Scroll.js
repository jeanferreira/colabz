import styled from 'styled-components/native';

export const Scroll = styled.ScrollView`
  flex: 1;
  background-color: #FFF;
  flex-direction: column;
  padding-top: 20px;
`;