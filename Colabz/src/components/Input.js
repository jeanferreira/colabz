import styled from 'styled-components/native';

export const Input = styled.TextInput`
  width: 300px;
  height: 55px;
  background-color: #FFF;
  border-radius: 8px;
  padding-left: 15px;
  margin: 15px 0;
  font-size: 12px;
  color: #444;
  elevation: 5;
  font-family: 'Montserrat Medium Italic';
`;
