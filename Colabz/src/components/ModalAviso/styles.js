import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: rgba(0,0,0,0.5);
  align-items: center;
  justify-content: center;
`;

export const Box = styled.View`
    width: 75%;
    background-color: #FFF;
    border-radius: 10px;
    elevation: 10;
    justify-content: center;
    align-items: center;
    /* padding: 0 30px; */
    padding-left: 30px;
    padding-right: 30px;
`;

export const Fechar = styled.TouchableOpacity`
  padding: 2px 20px;
`;