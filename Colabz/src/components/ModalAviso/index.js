import React, { Component } from 'react';
import { Button, TextButton } from '../Button';
import { TextBold, SmallText } from '../TextStyles';
import { Modal, StatusBar } from 'react-native';
import { Container, Box, Fechar } from './styles';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class ModalAviso extends Component {

    ocultarModal = () => {
        if(!this.props.liberado){
            this.props.setConfirmado(false);
            this.props.setStatusMoeda(false);
        }
        this.props.setVisibleModal(false);
    }

    render(){

        return (
            <>
            <StatusBar backgroundColor={this.props.backgroundStatusBar}/>
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.props.visibleModal}
                onRequestClose={() => this.ocultarModal()}
            >
                <Container>
                    <Box style={{paddingVertical: this.props.boxAltura}}>
                        <FontAwesome5
                            name={this.props.iconName}
                            solid={true} 
                            color={this.props.corIcon}
                            size={50}
                            style={{
                                marginBottom: 10,
                                display: this.props.visibilidadeIcone,
                            }}
                        />
                        <TextBold 
                            style={{
                                fontSize: 18, 
                                textAlign: 'center'
                            }}
                        >{this.props.mensagem}</TextBold>

                        <SmallText 
                            style={{ 
                                display: this.props.visibilidadeSubMensagem,
                                textAlign: 'center',
                                fontFamily: 'Montserrat Italic',
                            }}
                        >{this.props.subMensagem}</SmallText>

                        <Button
                            onPress={() => {this.props.actionButton()}} 
                            style={{
                                width: '100%', 
                                marginVertical: 20, 
                                backgroundColor: this.props.corBotao
                            }}
                        >
                            <TextButton style={{color: this.props.corTexto}}>{this.props.textoBotao}</TextButton>
                        </Button>

                        <Fechar onPress={() => this.props.close()}>
                            <TextButton style={{color: '#444', display: this.props.visibilidadeBotaoFechar}}>fechar</TextButton>
                        </Fechar>
                    </Box>
                </Container>
            </Modal>
            </>
        );
    }
}
