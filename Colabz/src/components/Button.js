import styled, { css } from 'styled-components/native';

export const Button = styled.TouchableOpacity`
    height: 60px;
    width: 300px;
    background-color: #FFF;
    border-radius: 50px;
    justify-content: center;
    align-items: center;
    elevation: 5;

    ${props => props.pink && css`
        background-color: #FF96A4;
    `}

    ${props => props.purple && css`
        background-color: #9F8CFF;
    `}

    ${props => props.black && css`
        font-family: 'Montserrat Bold';
    `}

    ${props => props.green && css`
        background-color: #75E4C4;
    `}
`;

export const TextButton = styled.Text`
    color: #FFF;
    font-size: 16px;
    font-family: "Montserrat SemiBold";

    ${props => props.black && css`
        color: #444;
    `}
`;

export const SimpleButton = styled.TouchableOpacity`
    width: 222px;
    height: 44px;
    background-color: #484848;
    justify-content: center;
    align-items: center;
    border-radius: 23px;
    flex-direction: row;
    margin-top: 10px;

    ${props => props.pink && css`
        background-color: #FF96A4;
    `}

    ${props => props.purple && css`
        background-color: #9F8CFF;
    `}

    ${props => props.white && css`
        background-color: #FFF;
        elevation: 4
    `}

    ${props => props.green && css`
        background-color: #75E4C4;
    `}

    ${props => props.large && css`
        width: 250px;
    `}

    ${props => props.overallWidth && css`
        width: 100%;
    `}

    ${props => props.boxShadow && css`
        elevation: 4;
    `}
`;