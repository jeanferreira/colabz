import styled, { css } from 'styled-components/native';

export const HightDesc = styled.Text`
    font-size: 35px;
    color: #444;
    font-family: 'Montserrat Bold';

    ${props => props.medium && css`
        font-size: 20px;
    `}
`;

export const TextNormal = styled.Text`
    font-family: 'Montserrat Medium';
    color: #444;

    ${props => props.bold && css`
        font-family: 'Montserrat Bold';
    `}

    ${props => props.semibold && css`
        font-family: 'Montserrat Medium';
    `}

    ${props => props.center && css`
        text-align: center;
    `}

    ${props => props.large && css`
        font-size: 15px;
    `}

    ${props => props.marginBottom && css`
        margin-bottom: 10px;
    `}

    ${props => props.marginVertical && css`
        margin: 10px 0;
    `}

    ${props => props.white && css`
        color: #FFF;
    `}
`;

export const SmallText = styled.Text`
    font-size: 12px;
    font-family: 'Montserrat Regular';
    color: #444;
    
    ${props => props.center && css`
        text-align: center;
    `}

    ${props => props.bold && css`
    font-family: 'Montserrat Bold';
    `}

    ${props => props.semibold && css`
        font-family: 'Montserrat Medium';
    `}

    ${props => props.marginVertical && css`
        margin: 5px 0;
    `}
`;

export const TextBold = styled.Text`
    font-size: 15px;
    font-family: 'Montserrat Bold';
    color: #444;
`;