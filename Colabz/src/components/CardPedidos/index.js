import React, { Component } from 'react';
import { Box, BoxTop, Indicador } from './styles';
import { SimpleButton, TextButton } from '../../components/Button';
import { TextNormal, SmallText } from '../TextStyles';

export default class CardPedidos extends Component {
  constructor(props){
    super(props);

    this.state = {
      status: 'novo',
      visibleStatus: 'flex',
      visibleLocation: 'flex',
      colorButton: '#9F8CFF',
      textButton: 'ver pedido do cliente',
      lines: 1,
      address: (this.props.address.length === 0 ? 'bairro não informado' : this.props.address),
      location: (this.props.location.length === 0 ? 'cidade e estado não informados' : this.props.location),
    }
  }

  componentDidMount(){
    if(this.props.email === 'wesleydias100@hotmail.com'){
      this.setState({
        status: 'aviso',
        colorButton: '#484848',
        textButton: 'visualizar aviso',
        visibleLocation: 'none',
        lines: 2,
      })
    }
  }

  render() {
    return(
      <Box>
        <BoxTop>
          <Indicador style={{display: this.state.visibleStatus}}>{this.state.status}</Indicador>
          
          <TextNormal bold numberOfLines={this.state.lines} style={{flex: 1 }}>{this.props.name}</TextNormal>
        </BoxTop>

        <SmallText style={{display: this.state.visibleLocation}}>{this.state.address}, {this.state.location}</SmallText>

        <SimpleButton overallWidth 
          style={{ backgroundColor: this.state.colorButton }}
          onPress={() => this.props.openDetailsProject()}
        >
          <TextButton>{this.state.textButton}</TextButton>
        </SimpleButton>
      </Box>
    );
  }
}
