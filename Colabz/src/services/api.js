import axios from 'axios';

const api = axios.create({
    baseURL: 'http://192.168.42.241:3333'
});

//'http://192.168.43.119:3333' CEL6->NOTBOOK&CEL7
//'http://192.168.137.1:3333' USB->NOTBOOK->HOST->CEL7
//'http://192.168.42.241:3333' CEL7->USB->NOTEBOOK

export default api;