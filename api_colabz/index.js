const express = require('express');
const routes = require('./routes');
const cors = require('cors');

const engines = require('consolidate');

const app = express();

app.engine("ejs", engines.ejs);
app.set("views","./src/views");
app.set("view engine", "ejs");

app.use(cors());
app.use(express.json());
app.use(routes);

app.listen(3333);