const conn = require('../config/connection');
const nodemailer = require('nodemailer');
const emailConfig = require('../config/email.json');

module.exports = {
    index(request, response) {
        const con = conn.connection();
        console.log('entrou em index');
        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const category_id = request.headers.category_id;
            const customer_id = request.headers.customer_id;
            const { page = 1 } = request.query;
            console.log(page);

            const intervalo = 5; //Constante que define a quantidade para paginação

            con.query('SELECT COUNT(*) AS qtd FROM lhch_project '+
            'INNER JOIN lhch_product_category_description ON lhch_product_category_description.product_category_id = lhch_project.product_category_id '+
            'WHERE lhch_project.product_category_id = ? '+
            'AND lhch_project.project_id NOT IN (SELECT lhch_project_to_customer.project_id FROM lhch_project_to_customer WHERE lhch_project_to_customer.customer_id = ?)',[category_id,customer_id], (err, row) => {
                if(err) throw err

                const [{ qtd }] = row;

                response.header('X-Total-Count',qtd);
            });

            con.query('SELECT '+
            'lhch_product_category_description.product_category_id, '+ 
            'lhch_product_category_description.name AS nameDescription, '+  
            'lhch_project.project_id, '+ 
            'lhch_project.name, '+ 
            'lhch_project.description, '+ 
            'lhch_project.address, '+ 
            'lhch_project.location, '+ 
            'DATE_FORMAT(lhch_project.date_end,"%d/%m/%Y") AS date_end, '+
            'DATE_FORMAT(lhch_project.date_added,"%d/%m/%Y") AS date_added, '+
            'lhch_customer.customer_id, '+ 
            'lhch_customer.firstname, '+ 
            'lhch_customer.lastname, '+ 
            'lhch_customer.phone, '+ 
            'lhch_customer.email, '+ 
            'lhch_customer.type AS customer_type, '+
            'lhch_br_city.nome AS city, '+
            'lhch_br_zone.nome AS uf '+
            'FROM lhch_project '+
            'INNER JOIN lhch_customer ON lhch_customer.customer_id = lhch_project.customer_id '+
            'INNER JOIN lhch_product_category_description ON lhch_product_category_description.product_category_id = lhch_project.product_category_id '+
            'INNER JOIN lhch_br_city ON lhch_br_city.id = lhch_customer.city '+
            'INNER JOIN lhch_br_zone ON lhch_br_zone.codigo_uf = lhch_br_city.codigo_uf '+
            'WHERE lhch_project.product_category_id = ? '+
            'AND lhch_project.project_id NOT IN (SELECT lhch_project_to_customer.project_id FROM lhch_project_to_customer WHERE lhch_project_to_customer.customer_id = ?) '+
            'LIMIT ? OFFSET ? ',[category_id,customer_id,intervalo,(page-1)*intervalo], (err, rows) => {
                if(err) throw err
                // console.log(rows);
                return response.json(rows);
            });
            console.log((page-1)*intervalo);
        });
    },

    show(request,response){
        const con = conn.connection();
        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const category_id = request.headers.category_id;
            const customer_id = request.headers.customer_id;

            con.query('SELECT COUNT(*) AS qtd FROM lhch_project '+
            'INNER JOIN lhch_product_category_description ON lhch_product_category_description.product_category_id = lhch_project.product_category_id '+
            'WHERE lhch_project.product_category_id = ? '+
            'AND lhch_project.project_id NOT IN (SELECT lhch_project_to_customer.project_id FROM lhch_project_to_customer WHERE lhch_project_to_customer.customer_id = ?)',[category_id,customer_id], (err, row) => {
                if(err) throw err

                const [{ qtd }] = row;

                return response.json([{
                    total: qtd
                }]);
            });
        });
    },

    send(request, response){
        const {professional, cliente} = request.body;

        let transporter = nodemailer.createTransport({
            host: emailConfig.host,
            port: emailConfig.port,
            secure: emailConfig.secure,
            auth: {
                user: emailConfig.email,
                pass: emailConfig.password
            }
        });

        transporter.sendMail({
            from: "Colabz - Encontre os melhores profissionais <contato@colabz.com.br>",
            to: cliente.email,
            subject: "Colabz - "+professional.firstname+" se interessou pelo seu pedido \""+cliente.resume+"\"",
            text: "Olá, "+cliente.firstname+".\n\n"+professional.firstname+" se interessou pelo seu pedido de > "+cliente.resume+", e em breve entrará em contato com você.\n\nCaso não queira esperar, entre em contato diretamente:\n\n"+professional.phone+"\n"+professional.email,
            html: ""
        }).then(message => {
            console.log(message);
        }).catch(err => {
            console.log(err);
        })
    }
}