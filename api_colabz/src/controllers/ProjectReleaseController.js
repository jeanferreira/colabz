const conn = require('../config/connection');

module.exports = {
    index(request, response) {
        const con = conn.connection();
        
        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const customer_id = request.headers.customer_id;
            const { page = 1 } = request.query;
            
            const intervalo = 5; //Constante que define a quantidade para paginação

            con.query('SELECT COUNT(*) AS qtd FROM lhch_project_to_customer WHERE customer_id = ?',[customer_id], (err, row) => {
                if(err) throw err

                const [{ qtd }] = row;

                response.header('X-Total-Count',qtd);
            });

            con.query('SELECT '+
                'lhch_project_to_customer.project_id, '+
                'lhch_project_to_customer.customer_id, '+
                'lhch_project.customer_id, '+
                'lhch_project_to_customer.approved, '+
                'lhch_project.product_category_id, '+
                'lhch_project.name, '+
                'lhch_project.description, '+
                'lhch_project.address, '+
                'DATE_FORMAT(lhch_project.date_end,"%d/%m/%Y") AS date_end, '+
                'DATE_FORMAT(lhch_project.date_added,"%d/%m/%Y") AS date_added, '+
                'lhch_project.location, '+
                'lhch_customer.firstname, '+
                'lhch_customer.lastname, '+
                'lhch_customer.phone, '+
                'lhch_customer.operadora, '+
                'lhch_customer.whatsapp, '+
                'lhch_customer.email, '+
                'lhch_br_city.nome AS city, '+
                'lhch_br_zone.nome AS uf '+
                'FROM lhch_project_to_customer '+ 
                'INNER JOIN lhch_project ON lhch_project.project_id = lhch_project_to_customer.project_id '+ 
                'INNER JOIN lhch_customer ON lhch_customer.customer_id = lhch_project.customer_id '+ 
                'INNER JOIN lhch_br_city ON lhch_br_city.id = lhch_customer.city '+ 
                'INNER JOIN lhch_br_zone ON lhch_br_zone.codigo_uf = lhch_br_city.codigo_uf '+ 
                'WHERE lhch_project_to_customer.customer_id = ? LIMIT ? OFFSET ?',[customer_id,intervalo,(page-1)*intervalo], (err, rows) => {

                    if(err) throw err

                    return response.json(rows);
                }
            );
        });
    },

    show(request,response){
        const con = conn.connection();
        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const customer_id = request.headers.customer_id;

            con.query('SELECT COUNT(*) AS qtd FROM lhch_project_to_customer WHERE customer_id = ?',[customer_id], (err, row) => {
                if(err) throw err

                const [{ qtd }] = row;

                return response.json([{
                    total: qtd
                }]);
            });
        });
    },
}