const conn = require('../config/connection');

module.exports = {
    store(request, response) {
        const con = conn.connection();
        
        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const {project_id, customer_id, customer_type} = request.body;

            con.query('INSERT INTO lhch_project_to_customer (project_id, customer_id, customer_type) VALUES (?, ?, ?)',[project_id, customer_id, customer_type], (err, row) => {
                if(err) throw err

                return response.json(row);
            });
        });
    },

    show(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const project_id = request.headers.project_id;

            con.query('SELECT COUNT(project_id) AS total FROM lhch_project_to_customer WHERE project_id = ?',[project_id], (err, res) => {
                return response.json(res);
            });
        });
    }
}