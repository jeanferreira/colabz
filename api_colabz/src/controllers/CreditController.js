const conn = require('../config/connection');

module.exports = {
    store(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const { description, customer_id } = request.body;

            con.query('INSERT INTO lhch_customer_credit (customer_credit_id, customer_id, invoice_id, amount, description, date_added) VALUES (NULL, ?, ?, ?, ?, current_timestamp())',[customer_id,'',20,description], (err, row) => {
                if(err) throw err
                
                return response.json({ success: true });
            });
        });
    },

    show(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const customer_id = request.headers.authorization;
            
            con.query('SELECT SUM(amount) AS total FROM lhch_customer_credit WHERE lhch_customer_credit.customer_id = ?',customer_id, (err, row) => {
                if(err) throw err
                console.log(row);
                return response.json(row);
            });
        });
    },

    delete(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const { description, customer_id } = request.headers;
            console.log(request.body);
            con.query('INSERT INTO lhch_customer_credit (customer_credit_id, customer_id, invoice_id, amount, description, date_added) VALUES (NULL, ?, ?, ?, ?, current_timestamp())',[customer_id,'',-20,description], (err, res) => {
                if(err) throw err
                
                return response.json(res);
            });
        });
    }
}