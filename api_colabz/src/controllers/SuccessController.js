const conn = require('../config/connection');
const paypal = require('paypal-rest-sdk');
const nodemailer = require('nodemailer');
const emailConfig = require('../config/email.json');

function insertInvoiceItem(invoice_id,payment){
    const con = conn.connection();

    con.connect((err) => {
        if(err) {
            console.log('Erro ao conectar ao banco de dados... '+err);
            return;
        }
        console.log('Conexão estabelecida');

        con.query('INSERT INTO lhch_invoice_item (invoice_item_id,invoice_id,title,description,tax_class_id,quantity,price,tax,discount) '+
        'VALUES (NULL,?,?,?,?,?,?,?,?)',[invoice_id,payment.transactions[0].item_list.items[0].name,payment.transactions[0].description,payment.payer.payer_info.tax_id,payment.transactions[0].item_list.items[0].quantity,payment.transactions[0].item_list.items[0].price,payment.transactions[0].item_list.items[0].tax,payment.transactions[0].amount.details.shipping_discount],(err, row) => {
            if(err) throw err
            
            return true;
        });
    })
}

function insertCredit(customer_id, invoice_id, description, value, email){
    const con = conn.connection();

    con.connect((err) => {
        if(err) {
            console.log('Erro ao conectar ao banco de dados...',err);
            return;
        }
        console.log('Conexão estabelecida');


        con.query('INSERT INTO lhch_customer_credit (customer_credit_id, customer_id, invoice_id, amount, description, date_added) VALUES (NULL, ?, ?, ?, ?, current_timestamp())',[customer_id,invoice_id,value,description], (err, row) => {
            if(err) throw err
        
            let transporter = nodemailer.createTransport({
                host: emailConfig.host,
                port: emailConfig.port,
                secure: emailConfig.secure,
                auth: {
                    user: emailConfig.email,
                    pass: emailConfig.password
                }
            });

            transporter.sendMail({
                from: "Colabz - Encontre os melhores profissionais <contato@colabz.com.br>",
                to: email,
                subject: "Colabz - Nova transação",
                text: "Olá, "+firstname+". \nUma nova transação foi feita em sua conta.\n\n "+value+" moedas colabz - "+description+"",
                html: ""
            }).then(message => {
                console.log(message);
            }).catch(err => {
                console.log(err);
            })
            
            return;
        });
    });
}

module.exports = {
    show(request, response) {
        const PayerID = request.query.PayerID;
        const paymentId = request.query.paymentId;
        const total = request.query.total;
        const customer_id = request.query.customer_id;
        const credit = request.query.credit;
        const description = request.query.description;
        const desc = String(description);
        const descriptionString = desc.replace(/_/g,' ');

        const execute_payment_json = {
            payer_id: PayerID,
            transactions: [
                {
                    amount: {
                        currency: 'BRL',
                        total: total
                    }
                }
            ]
        };

        paypal.payment.execute(paymentId, execute_payment_json, async (error, payment) => {
            if(error){
                console.log(error.response);
                throw error;
            } else {
                console.log('Get payment response');
                console.log(JSON.stringify(payment));

                const con = conn.connection();

                con.connect((err) => {
                    if(err) {
                        console.log('Erro ao conectar ao banco de dados... '+err);
                        return;
                    }
                    console.log('Conexão estabelecida');

                    con.query('SELECT customer_id, firstname, lastname, company, website, email FROM lhch_customer WHERE customer_id = ?',[customer_id], (err, row) => {
                        if(err) throw err

                        const [rowDataPakcet] = row;
                        const user = rowDataPakcet;

                        con.query('INSERT INTO lhch_invoice '+
                        'VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[0,customer_id,user.firstname,user.lastname,user.company,user.website,user.email,payment.payer.payer_info.first_name,payment.payer.payer_info.last_name,"",payment.payer.payer_info.shipping_address.line1,payment.transactions[0].item_list.shipping_address.line1,payment.payer.payer_info.shipping_address.city,payment.payer.payer_info.shipping_address.postal_code,payment.payer.payer_info.shipping_address.country_code,payment.payer.payer_info.shipping_address.state,payment.transactions[0].amount.total,payment.transactions[0].related_resources[0].sale.id,payment.transactions[0].item_list.items[0].name,payment.transactions[0].description,payment.transactions[0].amount.currency,payment.transactions[0].amount.total,"",4,1,'0000-00-00',payment.transactions[0].related_resources[0].sale.create_time,'0000-00-00'], (err, row) => {
                            if(err) throw err

                            const {insertId} = row;
                            const invoice_id = insertId;

                            insertInvoiceItem(invoice_id,payment);
                            insertCredit(customer_id,invoice_id,`Compra do ${descriptionString} via PayPal`,credit,user.email);
                            return insertId;
                        });
                    });
                })                
                
                response.render('success');
            }
        })
    }
}