const paypal = require('paypal-rest-sdk');
const paypalConfig = require('../config/paypal.json');

paypal.configure(paypalConfig);

module.exports = {
    index(request, response) {
        // const pacoteId = request.query.id;

        //logo abaixo deve ser feito a verificação se o pacote de fato existe
        //porem, nesse caso vamos usar o token do usuário, para confirmar que
        //seja uma requisição de um usuário, para então prosseguir.
        const { packageName, description, price, credit, customer_id } = request.query;
        const desc = String(description);
        const descriptionString = desc.replace(/_/g,' ');
        const pckn = String(packageName);
        const packageNameString = pckn.replace(/_/g,' ');

        const package = [{
            "name": packageNameString,
            "sku": 1,
            "price": price,
            "currency": "BRL",
            "quantity": 1
        }];

        const value = { "currency": "BRL", "total": price};  

        const json_payment = {
            intent: "sale",
            payer: { payment_method: "paypal" },
            redirect_urls: {
                return_url: `http://mysql669.umbler.com:3333/success?total=${price}&customer_id=${customer_id}&credit=${credit}&description=${description}`,
                cancel_url: "http://mysql669.umbler.com:3333/cancel"
            },
            transactions: [{
                item_list: { items: package },
                amount: value,
                description: descriptionString
            }] 
        };

        paypal.payment.create(json_payment, (error, payment) => {
            if(error) {
                console.warn(error);
                return response.json(error);
            } else {
                console.log('PAYMENT');
                console.log(payment);
                response.redirect(payment.links[1].href);
            }
        });
    },
}