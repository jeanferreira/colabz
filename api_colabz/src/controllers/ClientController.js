const conn = require('../config/connection');

module.exports = {
    show(request, response){
        const con = conn.connection();

        con.connect((err) => {
            if(err){
                console.log('Erro ao conectar ao banco de dados... ',err);
                return;
            }

            console.log('Conexão estabelecida;');

            const { email } = request.query;

            // COMANDOS PARA PEGAR DADOS DO BANCO DE DADOS
            con.query('SELECT firstname, lastname, type, category_id, name, phone, email FROM lhch_customer '+
                'INNER JOIN lhch_product_category_description ON lhch_product_category_description.product_category_id = lhch_customer.category_id '+
                'WHERE email = ?',email, (err, row) => {
                if(err) throw err

                return response.json(row);
            })
        });
    },
}