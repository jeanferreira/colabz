const nodemailer = require('nodemailer');
const conn = require('../config/connection');

module.exports = {
    show(request, response) {
        const con = conn.connection();

        try {
            con.connect((err) => {
                if(err) {
                    console.log('Erro ao conectar ao banco de dados...'+err);
                    return;
                }
    
                console.log('Conexão estabelecida');
    
                const { email } = request.query;
    
                con.query("SELECT firstname, email, salt FROM lhch_customer WHERE email = ? AND type = 1;",[email], (err, row) => {
                    if(err) throw err
                    
                    return response.json(row);
                });
            });
        } catch(error){
            return response.json({alert: 'erro ao conectar, verifique sua internet'});
        }
        
    },

    send(request, response){
        let transporter = nodemailer.createTransport({
            host: "smtp.umbler.com",
            port: 587,
            secure: false,
            auth: {
                user: "contato@colabz.com.br",
                pass: "wesley0204@"
            }
        });

        transporter.sendMail({
            from: "Colabz - Encontre os melhores profissionais <contato@colabz.com.br>",
            to: "jeanferracy@gmail.com",
            subject: "Testando nodemailer",
            text: "Ola, estou testando o nodemailer",
            html: ""
        }).then(message => {
            console.log(message);
        }).catch(err => {
            console.log(err);
        })
    }
}