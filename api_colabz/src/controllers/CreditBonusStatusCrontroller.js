const conn = require('../config/connection');

module.exports = {
    store(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const { customer_id, date_bonus, rate_status } = request.body;
            console.log(request.body);
            con.query('INSERT INTO lhch_credit_bonus_status (customer_id, date_bonus, rate_bonus) VALUES (?, ?, ?)',[customer_id, date_bonus, rate_status], (err, row) => {
                if(err) throw err
                
                return response.json({ success: true });
            });
        });
    },

    show(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const customer_id = request.headers.customer_id;
            
            con.query('SELECT * FROM lhch_credit_bonus_status WHERE customer_id = ?',customer_id, (err, row) => {
                if(err) throw err
                
                return response.json(row);
            });
        });
    },

    update(request, response) {
        const con = conn.connection();

        con.connect((err) => {
            if(err) {
                console.log('Erro ao conectar ao banco de dados...',err);
                return;
            }

            console.log('Conexão estabelecida');

            const { customer_id, date_bonus, rate_status } = request.body;

            con.query('UPDATE lhch_credit_bonus_status SET date_bonus = ?, rate_bonus = ? WHERE customer_id = ?',[date_bonus, rate_status, customer_id], (err, res) => {
                if(err) throw err
                
                return response.json(res);
            });
        });
    }
}