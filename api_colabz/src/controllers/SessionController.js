const conn = require('../config/connection');
const nodemailer = require('nodemailer');
const emailConfig = require('../config/email.json');

module.exports = {
    show(request, response) {
        const con = conn.connection();
        
        try {
            con.connect((err) => {
                if(err) {
                    console.log('Erro ao conectar ao banco de dados...',err);
                    return;
                }
    
                console.log('Conexão estabelecida');
    
                const { email, password } = request.body;
    
                con.query("SELECT * FROM lhch_customer WHERE email = ? AND password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1(?))))) AND type = 1;",[email,password], (err, row) => {
                    if(err) throw err
                    
                    return response.json(row);
                });
            });
        } catch(error){
            return response.json({alert: 'erro ao conectar, verifique sua internet'});
        }
    },

    update(request, response) {
        let transporter = nodemailer.createTransport({
            host: emailConfig.host,
            port: emailConfig.port,
            secure: emailConfig.secure,
            auth: {
                user: emailConfig.email,
                pass: emailConfig.password
            }
        });

        let an = '';
        let len = 9;
        an = an&&an.toLowerCase();
        var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
        for(;i++<len;){
            var r = Math.random()*(max-min)+min <<0;
            str += String.fromCharCode(r+=r>9?r<36?55:61:48);
        }

        const newPassword = str;
        const { email, salt, firstname } = request.body;
        
        const con = conn.connection();

        con.connect((err) => {
            console.log('Erro ao conectar ao banco de dados...',err);
            return;
        });

        console.log('Conexão estabelecida');

        con.query('UPDATE lhch_customer SET password = SHA1(CONCAT(? , SHA1(CONCAT(? , SHA1(?))))) WHERE email = ?',[salt,salt,newPassword,email], (err, row) => {
            if(err) throw err
            
            transporter.sendMail({
                from: "Colabz - Encontre os melhores profissionais <contato@colabz.com.br>",
                to: email,
                subject: "Colabz - Nova senha",
                text: "Olá, "+firstname+". \nSua nova senha de acesso é "+newPassword+". \nAcesse sua conta se preferir alterar. ",
                html: ""
            }).then(message => {
                console.log(message);
            }).catch(err => {
                console.log(err);
            })

            return response.json(row);
        });        
    }
}