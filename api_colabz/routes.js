const { Router } = require('express');
const ClientController = require('./src/controllers/ClientController');
const SessionController = require('./src/controllers/SessionController');
const ProjectController = require('./src/controllers/ProjectController');
const ProjectReleaseController = require('./src/controllers/ProjectReleaseController');
const CreditController = require('./src/controllers/CreditController');
const CreditBonusStatusController = require('./src/controllers/CreditBonusStatusCrontroller');
const ProposalController = require('./src/controllers/ProposalController');
const SuccessController = require('./src/controllers/SuccessController');
const CancelController = require('./src/controllers/CancelController');
const BuyController = require('./src/controllers/BuyController');
const EmailController = require('./src/controllers/EmailController');

const routes = Router();

routes.post('/sessions',SessionController.show);
routes.patch('/sessions',SessionController.update);

routes.get('/email',EmailController.show);

routes.get('/clients',ClientController.show);

routes.get('/projects/total',ProjectController.show);
routes.get('/projects',ProjectController.index);
routes.post('/projects/email',ProjectController.send);

routes.get('/projectsRelease/total',ProjectReleaseController.show);
routes.get('/projectsRelease',ProjectReleaseController.index);

routes.post('/proposals',ProposalController.store);
routes.get('/proposals',ProposalController.show);

routes.post('/credits',CreditController.store);
routes.get('/credit',CreditController.show);
routes.delete('/credits',CreditController.delete);

routes.post('/bonus',CreditBonusStatusController.store);
routes.get('/bonus',CreditBonusStatusController.show);
routes.patch('/bonus',CreditBonusStatusController.update);

routes.get('/buy',BuyController.index);
routes.get('/success',SuccessController.show);
routes.get('/cancel',CancelController.show);

module.exports = routes;